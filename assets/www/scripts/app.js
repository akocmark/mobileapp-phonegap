document.addEventListener('deviceready', onDeviceReady, true);

function onDeviceReady() {
	App.init();
	
	if (window.location.pathname == '/android_asset/www/login.html') {
		App.loadScript('login');
	}

	if (window.location.pathname == '/android_asset/www/splash.html') {
		App.loadScript('splash');
	}

}

var App = {
	hash : null,

	server : 'http://mark.koodidojo.com',

	user : {},

	init : function() {
		var self = this;

		// get user if previously logged in
		var user = JSON.parse(localStorage.getItem("USER"));
		if (!$.isEmptyObject(user)) {
			self.user =  user;
		}

		// App ajax event listeners
		self.ajaxSend();
		self.ajaxSuccess();

		// onpopstate listener
		self.onpopstate();
		
		// stop here on login page
		if (window.location.pathname == '/android_asset/www/login.html'
			|| window.location.pathname == '/android_asset/www/splash.html') {
			return false;
		}

		// get the url hash and load the javascript for the hash
		var script 	= self.getHash();
		script 		= script.substr(1);
		self.loadScript(script);

		// override anchor tags behaviour
		$('a').click(function() {
			var script = $(this).attr('href').substr(1);

			if ($.isEmptyObject(script)) {
				return false;
			}

			// same state
			if (script == self.getHash().substr(1)) {
				return false;
			}

			self.loadScript(script);
			return false;
		});

		// logout button listener
		$(document).on('click', '.logout', function() {
			if (confirm('Are you sure you want to logout?')) {
				App.logout();
			}
		});
	},

	getHash : function() {
		return !$.isEmptyObject(window.location.hash) ? window.location.hash : '#home';
	},

	isLogin : function() {
		var self = this;
		if (!$.isEmptyObject(self.user.session_id)) {
			return true;
		}

		return false;
	},

	login : function(email, password) {
		var self = this;
		$.ajax({
     		crossDomain: true,
	    	type : 'POST',
	  		url : App.server+"/api/login",
	  		data : {
	  			email: email,
	  			password: password
	  		},
	  		success : function(data) {
	  			data = JSON.parse(data);
	  			if(!data.error) {
	  				this.user = data.user;
					localStorage.setItem("USER", JSON.stringify(data.user));

					window.location.href = '/android_asset/www/index.html';
				}

	  		},
	  		
			error: function (xhr, ajaxOptions, thrownError) { 
	  			alert('Unable to login.');
				alert("errorstatus: " + xhr.status + " ajaxoptions: " + ajaxOptions + " throwError: " + thrownError);
			}
	  	});

		return;
	},

	logout : function() {	
		localStorage.removeItem("USER");
		window.location.href = '/android_asset/www/login.html';
	},

	loadView : function(template, context) {
		$.ajax({
			redirect : true,
			type: 'GET',
			url : 'views/'+template+'.html',
			success : function(data) {
				var template = Handlebars.compile(data);
				var html = template(context);

				$('#main-content').html(html);
			}
		});
	},

	loadScript : function(src) {
		// if script is previously loaded
		var script = src.charAt(0).toUpperCase() + src.slice(1);
		if (window[script]) {
			window[script].init();
			history.pushState({script: src}, null, '#'+src);
			return true;
		}

		// load the script according to the hash
		var script 	= document.createElement('script');
		script.type = 'text/javascript';
		script.src 	= 'scripts/models/'+src+'.js';
		document.getElementsByTagName("head")[0].appendChild(script);

		history.pushState({script: src}, null, '#'+src);
	},

	onpopstate : function() {
		var self = this;
		window.onpopstate = function(event) {
			var script = event.state ? event.state.script : '';
			if (!$.isEmptyObject(script)) {
				self.loadScript(script);
			}
		}
	},

	registerPartials : function(partials) {
		var self = this;
		for (var i in partials) {
			self.registerPartial(partials[i]);
		}
	},

	registerPartial : function(partial) {
		var self = this;

		self.getPartial(partial, function(data) {
  			if (!$.isEmptyObject(data)) {
				Handlebars.registerPartial(partial, data);
  			}
		});
	},

	getPartial : function(partial, callback) {
		$.ajax({
			internal: true,
	    	type : 'GET',
	  		url : '/android_asset/www/partials/'+partial+'.html',
	  		success : function(data) {
	  			callback(data);
	  		}
	  	});
	},

	ajaxSend : function() {
		$(document).ajaxSend(function(event, jqxhr, settings) {
			if (isset(settings.redirect) || isset(settings.internal)) {
				return;
			}

			if (settings.type == 'GET') {
				var tmp = settings.url.split('?');
				var conjunctor = tmp.length > 1 ? '&' : '?';
				settings.url += conjunctor  + '_id=' + App.user.id + '&session_id=' + App.user.session_id;
				return;
			}

			if (!isset(settings.data)) {
				settings.data = '';
			}

			var conjunctor = !$.isEmptyObject(settings.data) ? '&' : '';
			settings.data += conjunctor + '_id=' + App.user.id + '&session_id=' + App.user.session_id;
		});
	},

	ajaxSuccess : function() {
		$( document ).ajaxSuccess(function( event, xhr, settings ) {
			if (isset(settings.redirect) || isset(settings.internal)) {
				return;
			}

			var response = JSON.parse(xhr.responseText);
			if (response.error) {
				alert(response.message);
			}
		});
	},

	cache : {},

	cacheContent : function(cacheName) {
		this.cache[cacheName] = $('#main-content').html();
	}
};

/* Helper functions */
var isset = function(obj) {
	if (typeof obj !== 'undefined') {
		return true;
	}

	return false;
}