var Home = {
	page : null,

	init : function() {
		var self = this;

		self.page = 1;

		// register partials to be used
		App.registerPartials(['posts']);

		// fetch posts via on the server
		self.fetchPosts(self.page, function(data) {
			//load view
			App.loadView('home', data);
		});

		// autload posts on scroll
		self.postAutoload();
	},

	postAutoload : function() {
		var self 			= this;
	  	var allowAutoload 	= true;

		$(window).scroll(function() {
			if (App.getHash() !== '#home') {
				return false;
			}

		    if (allowAutoload && ($(window).scrollTop() >  $(document).height() - $(window).height() - 400)) {
		        allowAutoload = false;

		        // add a loader
		        $('.posts').append('<div class="post-loader" style="text-align: center">Loading..</div>');

		        // autoload content
				App.getPartial('posts', function(partial) {
		  			self.page++;

					// fetch posts via on the server
					self.fetchPosts(self.page, function(data) {
						// compile partial
						var template 	= Handlebars.compile(partial);
						var html 		= template(data);
						
						//append posts
						$('.posts').append(html);
						$('.post-loader').remove();

						// cache the page result
						App.cacheContent('home');

						// reset allowAutoload flag if posts count is >= 20
						if (data.posts.length >= 8) {
							allowAutoload = true;
						} else {
	       					$('.posts').append('<div class="post-loader" style="text-align: center">No more posts to load.</div>');
						}
					});
				});
		    }
		});
	},

	fetchPosts : function(page, callback) {
		$.ajax({
	    	type : 'GET',
	    	data : {
	    		page: page
	    	},
	  		url : App.server+"/api/v1/posts",
	  		success : function(data) {
	  			data = JSON.parse(data);
	  			callback(data);
	  		}
	  	});
	},

	onBackKeyDown : function() {   
		navigator.app.exitApp();
	}

};

document.addEventListener("backbutton", Home.onBackKeyDown, false);
Home.init();