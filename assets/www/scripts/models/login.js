var Login = {

	init : function() {
		$('button.login').click(function(e) {
			e.preventDefault();

			App.login(
				$('input[name="email"]').val(),
				$('input[name="password"]').val()
			);

			return false;
		});
	},

	onBackKeyDown : function() {   
		navigator.app.exitApp();
	}

};

document.addEventListener("backbutton", Login.onBackKeyDown, false);
Login.init();