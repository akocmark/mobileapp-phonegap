var More = {
	init : function() {
		// compile partial
		App.getPartial('more', function(partial) {
			var template 	= Handlebars.compile(partial);
			var html 		= template(App.user);

			$('#main-content').html(html);
		});
	}
};

More.init();
