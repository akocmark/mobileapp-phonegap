var Splash = {

	init : function() {
		if (App.isLogin()) {
			window.location.href = '/android_asset/www/index.html';
		} else {
			window.location.href = '/android_asset/www/login.html';
		}
	},

	onBackKeyDown : function() {   
		navigator.app.exitApp();
	}

};

document.addEventListener("backbutton", Splash.onBackKeyDown, false);
Splash.init();